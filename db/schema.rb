# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_03_195628) do

  create_table "caja_generals", force: :cascade do |t|
    t.string "tipo"
    t.string "entrada"
    t.string "salida"
    t.string "descripcion"
    t.datetime "fecha"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "caja_registradoras", force: :cascade do |t|
    t.string "tipo"
    t.string "entrada"
    t.string "salida"
    t.string "descripcion"
    t.datetime "fecha"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "categoria", force: :cascade do |t|
    t.string "nombre"
    t.integer "fecha"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string "nombre"
    t.integer "telefono"
    t.string "email"
    t.datetime "fecha_cumpleaños"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "empleados", force: :cascade do |t|
    t.string "nombre"
    t.integer "telefono"
    t.string "email"
    t.datetime "fecha_cumpleanos"
    t.datetime "fecha_ingreso"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "inventarios", force: :cascade do |t|
    t.string "producto"
    t.integer "cantidad"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "productos", force: :cascade do |t|
    t.string "nombre"
    t.string "descripcion"
    t.datetime "fecha_vencimiento"
    t.string "precio"
    t.string "categoria"
    t.string "proveedor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "proveedores", force: :cascade do |t|
    t.string "nombre"
    t.integer "telefono"
    t.string "email"
    t.string "nombre_empresa"
    t.string "tipo_proveedor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
