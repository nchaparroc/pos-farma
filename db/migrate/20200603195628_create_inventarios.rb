class CreateInventarios < ActiveRecord::Migration[6.0]
  def change
    create_table :inventarios do |t|
      t.string :producto
      t.integer :cantidad

      t.timestamps
    end
  end
end
