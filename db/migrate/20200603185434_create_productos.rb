class CreateProductos < ActiveRecord::Migration[6.0]
  def change
    create_table :productos do |t|
      t.string :nombre
      t.string :descripcion
      t.datetime :fecha_vencimiento
      t.string :precio
      t.string :categoria
      t.string :proveedor

      t.timestamps
    end
  end
end
