class CreateClientes < ActiveRecord::Migration[6.0]
  def change
    create_table :clientes do |t|
      t.string :nombre
      t.integer :telefono
      t.string :email
      t.datetime :fecha_cumpleaños

      t.timestamps
    end
  end
end
