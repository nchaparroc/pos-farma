class CreateEmpleados < ActiveRecord::Migration[6.0]
  def change
    create_table :empleados do |t|
      t.string :nombre
      t.integer :telefono
      t.string :email
      t.datetime :fecha_cumpleanos
      t.datetime :fecha_ingreso

      t.timestamps
    end
  end
end
