class CreateProveedores < ActiveRecord::Migration[6.0]
  def change
    create_table :proveedores do |t|
      t.string :nombre
      t.integer :telefono
      t.string :email
      t.string :nombre_empresa
      t.string :tipo_proveedor

      t.timestamps
    end
  end
end
