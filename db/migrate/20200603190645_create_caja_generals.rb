class CreateCajaGenerals < ActiveRecord::Migration[6.0]
  def change
    create_table :caja_generals do |t|
      t.string :tipo
      t.string :entrada
      t.string :salida
      t.string :descripcion
      t.datetime :fecha

      t.timestamps
    end
  end
end
