require "application_system_test_case"

class CajaGeneralsTest < ApplicationSystemTestCase
  setup do
    @caja_general = caja_generals(:one)
  end

  test "visiting the index" do
    visit caja_generals_url
    assert_selector "h1", text: "Caja Generals"
  end

  test "creating a Caja general" do
    visit caja_generals_url
    click_on "New Caja General"

    fill_in "Descripcion", with: @caja_general.descripcion
    fill_in "Entrada", with: @caja_general.entrada
    fill_in "Fecha", with: @caja_general.fecha
    fill_in "Salida", with: @caja_general.salida
    fill_in "Tipo", with: @caja_general.tipo
    click_on "Create Caja general"

    assert_text "Caja general was successfully created"
    click_on "Back"
  end

  test "updating a Caja general" do
    visit caja_generals_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @caja_general.descripcion
    fill_in "Entrada", with: @caja_general.entrada
    fill_in "Fecha", with: @caja_general.fecha
    fill_in "Salida", with: @caja_general.salida
    fill_in "Tipo", with: @caja_general.tipo
    click_on "Update Caja general"

    assert_text "Caja general was successfully updated"
    click_on "Back"
  end

  test "destroying a Caja general" do
    visit caja_generals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Caja general was successfully destroyed"
  end
end
