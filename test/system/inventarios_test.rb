require "application_system_test_case"

class InventariosTest < ApplicationSystemTestCase
  setup do
    @inventario = inventarios(:one)
  end

  test "visiting the index" do
    visit inventarios_url
    assert_selector "h1", text: "Inventarios"
  end

  test "creating a Inventario" do
    visit inventarios_url
    click_on "New Inventario"

    fill_in "Cantidad", with: @inventario.cantidad
    fill_in "Producto", with: @inventario.producto
    click_on "Create Inventario"

    assert_text "Inventario was successfully created"
    click_on "Back"
  end

  test "updating a Inventario" do
    visit inventarios_url
    click_on "Edit", match: :first

    fill_in "Cantidad", with: @inventario.cantidad
    fill_in "Producto", with: @inventario.producto
    click_on "Update Inventario"

    assert_text "Inventario was successfully updated"
    click_on "Back"
  end

  test "destroying a Inventario" do
    visit inventarios_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Inventario was successfully destroyed"
  end
end
