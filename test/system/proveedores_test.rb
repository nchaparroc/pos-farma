require "application_system_test_case"

class ProveedoresTest < ApplicationSystemTestCase
  setup do
    @proveedore = proveedores(:one)
  end

  test "visiting the index" do
    visit proveedores_url
    assert_selector "h1", text: "Proveedores"
  end

  test "creating a Proveedore" do
    visit proveedores_url
    click_on "New Proveedore"

    fill_in "Email", with: @proveedore.email
    fill_in "Nombre", with: @proveedore.nombre
    fill_in "Nombre empresa", with: @proveedore.nombre_empresa
    fill_in "Telefono", with: @proveedore.telefono
    fill_in "Tipo proveedor", with: @proveedore.tipo_proveedor
    click_on "Create Proveedore"

    assert_text "Proveedore was successfully created"
    click_on "Back"
  end

  test "updating a Proveedore" do
    visit proveedores_url
    click_on "Edit", match: :first

    fill_in "Email", with: @proveedore.email
    fill_in "Nombre", with: @proveedore.nombre
    fill_in "Nombre empresa", with: @proveedore.nombre_empresa
    fill_in "Telefono", with: @proveedore.telefono
    fill_in "Tipo proveedor", with: @proveedore.tipo_proveedor
    click_on "Update Proveedore"

    assert_text "Proveedore was successfully updated"
    click_on "Back"
  end

  test "destroying a Proveedore" do
    visit proveedores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Proveedore was successfully destroyed"
  end
end
