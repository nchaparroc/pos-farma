require "application_system_test_case"

class CajaRegistradorasTest < ApplicationSystemTestCase
  setup do
    @caja_registradora = caja_registradoras(:one)
  end

  test "visiting the index" do
    visit caja_registradoras_url
    assert_selector "h1", text: "Caja Registradoras"
  end

  test "creating a Caja registradora" do
    visit caja_registradoras_url
    click_on "New Caja Registradora"

    fill_in "Descripcion", with: @caja_registradora.descripcion
    fill_in "Entrada", with: @caja_registradora.entrada
    fill_in "Fecha", with: @caja_registradora.fecha
    fill_in "Salida", with: @caja_registradora.salida
    fill_in "Tipo", with: @caja_registradora.tipo
    click_on "Create Caja registradora"

    assert_text "Caja registradora was successfully created"
    click_on "Back"
  end

  test "updating a Caja registradora" do
    visit caja_registradoras_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @caja_registradora.descripcion
    fill_in "Entrada", with: @caja_registradora.entrada
    fill_in "Fecha", with: @caja_registradora.fecha
    fill_in "Salida", with: @caja_registradora.salida
    fill_in "Tipo", with: @caja_registradora.tipo
    click_on "Update Caja registradora"

    assert_text "Caja registradora was successfully updated"
    click_on "Back"
  end

  test "destroying a Caja registradora" do
    visit caja_registradoras_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Caja registradora was successfully destroyed"
  end
end
