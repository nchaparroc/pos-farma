require 'test_helper'

class CajaGeneralsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @caja_general = caja_generals(:one)
  end

  test "should get index" do
    get caja_generals_url
    assert_response :success
  end

  test "should get new" do
    get new_caja_general_url
    assert_response :success
  end

  test "should create caja_general" do
    assert_difference('CajaGeneral.count') do
      post caja_generals_url, params: { caja_general: { descripcion: @caja_general.descripcion, entrada: @caja_general.entrada, fecha: @caja_general.fecha, salida: @caja_general.salida, tipo: @caja_general.tipo } }
    end

    assert_redirected_to caja_general_url(CajaGeneral.last)
  end

  test "should show caja_general" do
    get caja_general_url(@caja_general)
    assert_response :success
  end

  test "should get edit" do
    get edit_caja_general_url(@caja_general)
    assert_response :success
  end

  test "should update caja_general" do
    patch caja_general_url(@caja_general), params: { caja_general: { descripcion: @caja_general.descripcion, entrada: @caja_general.entrada, fecha: @caja_general.fecha, salida: @caja_general.salida, tipo: @caja_general.tipo } }
    assert_redirected_to caja_general_url(@caja_general)
  end

  test "should destroy caja_general" do
    assert_difference('CajaGeneral.count', -1) do
      delete caja_general_url(@caja_general)
    end

    assert_redirected_to caja_generals_url
  end
end
