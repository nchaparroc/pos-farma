require 'test_helper'

class CajaRegistradorasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @caja_registradora = caja_registradoras(:one)
  end

  test "should get index" do
    get caja_registradoras_url
    assert_response :success
  end

  test "should get new" do
    get new_caja_registradora_url
    assert_response :success
  end

  test "should create caja_registradora" do
    assert_difference('CajaRegistradora.count') do
      post caja_registradoras_url, params: { caja_registradora: { descripcion: @caja_registradora.descripcion, entrada: @caja_registradora.entrada, fecha: @caja_registradora.fecha, salida: @caja_registradora.salida, tipo: @caja_registradora.tipo } }
    end

    assert_redirected_to caja_registradora_url(CajaRegistradora.last)
  end

  test "should show caja_registradora" do
    get caja_registradora_url(@caja_registradora)
    assert_response :success
  end

  test "should get edit" do
    get edit_caja_registradora_url(@caja_registradora)
    assert_response :success
  end

  test "should update caja_registradora" do
    patch caja_registradora_url(@caja_registradora), params: { caja_registradora: { descripcion: @caja_registradora.descripcion, entrada: @caja_registradora.entrada, fecha: @caja_registradora.fecha, salida: @caja_registradora.salida, tipo: @caja_registradora.tipo } }
    assert_redirected_to caja_registradora_url(@caja_registradora)
  end

  test "should destroy caja_registradora" do
    assert_difference('CajaRegistradora.count', -1) do
      delete caja_registradora_url(@caja_registradora)
    end

    assert_redirected_to caja_registradoras_url
  end
end
