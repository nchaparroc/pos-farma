Rails.application.routes.draw do
  resources :inventarios
  resources :caja_registradoras
  resources :caja_generals
  resources :empleados
  resources :proveedores
  resources :clientes
  resources :productos
  resources :categoria
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get '/home', to: 'application#home' 

  root to: "caja_registradoras#index"
end
