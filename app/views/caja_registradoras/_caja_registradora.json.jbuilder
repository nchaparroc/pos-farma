json.extract! caja_registradora, :id, :tipo, :entrada, :salida, :descripcion, :fecha, :created_at, :updated_at
json.url caja_registradora_url(caja_registradora, format: :json)
