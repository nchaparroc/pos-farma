json.extract! caja_general, :id, :tipo, :entrada, :salida, :descripcion, :fecha, :created_at, :updated_at
json.url caja_general_url(caja_general, format: :json)
