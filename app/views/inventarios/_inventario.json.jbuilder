json.extract! inventario, :id, :producto, :cantidad, :created_at, :updated_at
json.url inventario_url(inventario, format: :json)
