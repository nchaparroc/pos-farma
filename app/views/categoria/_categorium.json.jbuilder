json.extract! categorium, :id, :nombre, :fecha, :created_at, :updated_at
json.url categorium_url(categorium, format: :json)
