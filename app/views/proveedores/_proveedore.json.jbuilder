json.extract! proveedore, :id, :nombre, :telefono, :email, :nombre_empresa, :tipo_proveedor, :created_at, :updated_at
json.url proveedore_url(proveedore, format: :json)
