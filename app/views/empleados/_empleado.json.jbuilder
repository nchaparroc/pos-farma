json.extract! empleado, :id, :nombre, :telefono, :email, :fecha_cumpleanos, :fecha_ingreso, :created_at, :updated_at
json.url empleado_url(empleado, format: :json)
