class CajaRegistradorasController < ApplicationController
  before_action :set_caja_registradora, only: [:show, :edit, :update, :destroy]

  # GET /caja_registradoras
  # GET /caja_registradoras.json
  def index
    @caja_registradoras = CajaRegistradora.all
  end

  # GET /caja_registradoras/1
  # GET /caja_registradoras/1.json
  def show
  end

  # GET /caja_registradoras/new
  def new
    @caja_registradora = CajaRegistradora.new
  end

  # GET /caja_registradoras/1/edit
  def edit
  end

  # POST /caja_registradoras
  # POST /caja_registradoras.json
  def create
    @caja_registradora = CajaRegistradora.new(caja_registradora_params)

    respond_to do |format|
      if @caja_registradora.save
        format.html { redirect_to @caja_registradora, notice: 'Caja registradora was successfully created.' }
        format.json { render :show, status: :created, location: @caja_registradora }
      else
        format.html { render :new }
        format.json { render json: @caja_registradora.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /caja_registradoras/1
  # PATCH/PUT /caja_registradoras/1.json
  def update
    respond_to do |format|
      if @caja_registradora.update(caja_registradora_params)
        format.html { redirect_to @caja_registradora, notice: 'Caja registradora was successfully updated.' }
        format.json { render :show, status: :ok, location: @caja_registradora }
      else
        format.html { render :edit }
        format.json { render json: @caja_registradora.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /caja_registradoras/1
  # DELETE /caja_registradoras/1.json
  def destroy
    @caja_registradora.destroy
    respond_to do |format|
      format.html { redirect_to caja_registradoras_url, notice: 'Caja registradora was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_caja_registradora
      @caja_registradora = CajaRegistradora.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def caja_registradora_params
      params.require(:caja_registradora).permit(:tipo, :entrada, :salida, :descripcion, :fecha)
    end
end
