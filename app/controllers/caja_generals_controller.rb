class CajaGeneralsController < ApplicationController
  before_action :set_caja_general, only: [:show, :edit, :update, :destroy]

  # GET /caja_generals
  # GET /caja_generals.json
  def index
    @caja_generals = CajaGeneral.all
  end

  # GET /caja_generals/1
  # GET /caja_generals/1.json
  def show
  end

  # GET /caja_generals/new
  def new
    @caja_general = CajaGeneral.new
  end

  # GET /caja_generals/1/edit
  def edit
  end

  # POST /caja_generals
  # POST /caja_generals.json
  def create
    @caja_general = CajaGeneral.new(caja_general_params)

    respond_to do |format|
      if @caja_general.save
        format.html { redirect_to @caja_general, notice: 'Caja general was successfully created.' }
        format.json { render :show, status: :created, location: @caja_general }
      else
        format.html { render :new }
        format.json { render json: @caja_general.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /caja_generals/1
  # PATCH/PUT /caja_generals/1.json
  def update
    respond_to do |format|
      if @caja_general.update(caja_general_params)
        format.html { redirect_to @caja_general, notice: 'Caja general was successfully updated.' }
        format.json { render :show, status: :ok, location: @caja_general }
      else
        format.html { render :edit }
        format.json { render json: @caja_general.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /caja_generals/1
  # DELETE /caja_generals/1.json
  def destroy
    @caja_general.destroy
    respond_to do |format|
      format.html { redirect_to caja_generals_url, notice: 'Caja general was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_caja_general
      @caja_general = CajaGeneral.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def caja_general_params
      params.require(:caja_general).permit(:tipo, :entrada, :salida, :descripcion, :fecha)
    end
end
